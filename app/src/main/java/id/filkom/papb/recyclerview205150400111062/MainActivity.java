package id.filkom.papb.recyclerview205150400111062;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

import java.util.ArrayList;

public class MainActivity extends AppCompatActivity  implements View.OnClickListener{

    RecyclerView rv1;
    EditText etNim, editTextTextPersonName2;
    Button bt1;
    public static String TAG = "RV1";


    ArrayList<Mahasiswa> data;
    MahasiswaAdapter adapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        rv1 = findViewById(R.id.rv1);
        bt1 = findViewById(R.id.bt1);
        etNim = findViewById(R.id.etNim);
        editTextTextPersonName2 = findViewById(R.id.editTextTextPersonName2);
        data = new ArrayList<>();
        adapter = new MahasiswaAdapter(this, data);
        rv1.setAdapter(adapter);
        rv1.setLayoutManager(new LinearLayoutManager(this));
        bt1.setOnClickListener(this);
    }

    @Override
    public void onClick(View view){
        Mahasiswa mahasiswa = new Mahasiswa();
        mahasiswa.nama =editTextTextPersonName2.getText().toString();
        mahasiswa.nim =etNim.getText().toString();
        data.add(mahasiswa);
        adapter.notifyDataSetChanged();
    }

}