package id.filkom.papb.recyclerview205150400111062;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.widget.TextView;



public class Activity2 extends AppCompatActivity{

    TextView tvNama2, tvNim2;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_2);
        tvNama2 = findViewById(R.id.tvNama2);
        tvNim2 = findViewById(R.id.tvNim2);

        Intent intent= getIntent();
        String name =intent.getStringExtra("Nama");
        String nim =intent.getStringExtra("NIM");

        tvNama2.setText("Nama : "+name);
        tvNim2.setText("NIM : "+nim);
    }

}